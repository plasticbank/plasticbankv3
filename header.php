<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package plasticbank
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'plasticbank' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="pb-header-container">
			<div class="site-branding">
				<?php
				the_custom_logo();
				?>
			</div><!-- .site-branding -->

			<div class="pb-header-dynamic">
				<nav id="site-navigation" class="main-navigation">
					<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'plasticbank' ); ?></button>
					<?php
					wp_nav_menu(
						array(
							'theme_location' => 'menu-1',
							'menu_id'        => 'primary-menu',
						)
					);
					?>
				</nav><!-- #site-navigation -->
				<div class="pb-header-dynamic-control">
					<div class="header-cart-control" id="header-cart-control">
						<a href="#" class="cart-toggle">
						<?php
							$cart_contents_count = WC()->cart->cart_contents_count;
							if($cart_contents_count > 0){
						?>
							<span class="cart-counter"><?php echo $cart_contents_count; ?></span>
						<?php
							}
						?>
						</a>
					</div>
				<?php
					ob_start();
					$account_url = get_permalink( get_page_by_path( 'account' ) );
					$login_url   = get_permalink( get_page_by_path( 'login' ) );
					if ( is_user_logged_in() ) {
						$current_user = wp_get_current_user();
						?>
						<div class="header-user-control logged-in">
							<a class="header-login logged-in" href="<?php echo $account_url; ?>">
								<span class="avatar">
								<?php echo get_avatar( $current_user->ID, 36 ); ?>
								</span>
							</a>
						</div>
						<?php
					} else {
						$login_url = get_permalink( get_page_by_path( 'login' ) );
						?>
						<div class="header-user-control">
							<a class="header-login" href="#">
								<i class="far fa-user"></i>
							</a>
						</div>
						<?php
					}
				?>
				</div>
			</div>
		</div>
	</header><!-- #masthead -->
