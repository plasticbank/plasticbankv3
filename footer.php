<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package plasticbank
 */

?>
	<div class="pb-cart-overlay"></div>
	<div class="pb-cart-sidebar">
		<div class="pb-cart-title">
			<span>My Cart</span>
			<a class="pb-cart-hide-toggle" href="#"></a>
		</div>
		<div class="pb-cart-body">
	</div>
		<div class="pb-cart-footer">
		</div>
	</div>

	<footer id="colophon" class="site-footer">
		<div class="site-info">
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'plasticbank' ) ); ?>">
				<?php
				/* translators: %s: CMS name, i.e. WordPress. */
				printf( esc_html__( 'Proudly powered by %s', 'plasticbank' ), 'WordPress' );
				?>
			</a>
			<span class="sep"> | </span>
				<?php
				/* translators: 1: Theme name, 2: Theme author. */
				printf( esc_html__( 'Theme: %1$s by %2$s.', 'plasticbank' ), 'plasticbank', '<a href="http://underscores.me/">Digihub</a>' );
				?>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
