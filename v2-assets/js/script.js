jQuery(document).ready(function() {                               
	jQuery(".owl-carousel-team").owlCarousel({
		autoPlay: 3000, //Set AutoPlay to 3 seconds
		items : 3,
		// navigation : true, // Show next and prev buttons
		loop:true,
		margin:30,
		nav:true,
		navText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
		responsive:{
			0:{
				items:1
			},
			600:{
				items:2
			},
			1000:{
				items:3
			}
		}
    });          
});