( function( $ ) {
  $( document ).ready(function() {
 
    $("#label_58_3_0").after(function() {
      return "<ul style='padding-left: 20px; font-size: 13px; margin-top: 10px; font-style: italic;'><li>Any site member can join this group.</li><li>This group will be listed in the groups directory and in search results.</li><li>Group content and activity will be visible to any site member.</li></ul>";
    });
    $("#label_58_3_1").after(function() {
      return "<ul style='padding-left: 20px; font-size: 13px; margin-top: 10px; font-style: italic;'><li>Only users who request membership and are accepted can join the group.</li><li>This group will be listed in the groups directory and in search results.</li><li>Group content and activity will only be visible to members of the group.</li></ul>";
    });
    $("#label_58_3_2").after(function() {
      return "<ul style='padding-left: 20px; font-size: 13px; margin-top: 10px; font-style: italic;'><li>Only users who are invited can join the group.</li><li>This group will not be listed in the groups directory or search results.</li><li>Group content and activity will only be visible to members of the group.</li></ul>";
    });
	$( "#signup" ).click(function() {
		console.log('signup');
        $('#form-login').css("display", "none");
        $('#form-register').css("display", "block");
    });
	$( "#signin" ).click(function() {
		console.log('signin');
        $('#form-login').css("display", "block");
        $('#form-register').css("display", "none");
    });
	
    var substringMatcher = function(strs) {
      console.log('58');
      return function findMatches(q, cb) {
        var matches, substringRegex;
    
        // an array that will be populated with substring matches
        matches = [];
    
        // regex used to determine if a string contains the substring `q`
        substrRegex = new RegExp(q, 'i');
    
        // iterate through the pool of strings and for any string that
        // contains the substring `q`, add it to the `matches` array
        $.each(strs, function(i, str) {
          if (substrRegex.test(str)) {
            matches.push(str);
          }
        });
    
        cb(matches);
      };
    };
    
    $.get('/wp-admin/admin-ajax.php', {action:'get_workplace'}, function (data, textStatus, jqXHR) {
      // $('p').append(data.firstName);
      console.log(data);

      var faith = $('#input_58_7').val();
      //var states = data.data.group_lists[faith];
      var states = data.data.all_group_lists;
      
	  console.log(states);
	  
      $( "#input_58_7" ).change(function() {
        faith = $('#input_58_7').val();
        //var states = data.data.group_lists[faith];
        var states = data.data.all_group_lists;

        $('.typeahead #input_58_12').typeahead('destroy');
        $('.typeahead #input_58_12').typeahead({
          hint: true,
          highlight: true,
          minLength: 1
        },
        {
          name: 'states',
          source: substringMatcher(states),
          templates: {
            empty: function(context){
              $(".tt-dataset").text('No Results Found');
            }
          }
        });
      });

      $('.typeahead #input_58_12').typeahead({
        hint: false,
        highlight: true,
        minLength: 1
      },
      {
        name: 'states',
        source: substringMatcher(states),
        templates: {
          empty: function(context){
            $(".tt-dataset").text('No Results Found');
          }
        }
      });
      
      var $myTypeahead = $(".typeahead #input_58_12");
      var selected      = null;
      var originalVal   = null;
      $myTypeahead.on("typeahead:active", function(aEvent) {
        selected       = null;
        originalVal    = $myTypeahead.typeahead("val");
      })
      $myTypeahead.on("typeahead:select", function(aEvent, aSuggestion) {
        selected = aSuggestion;
      });
      $myTypeahead.on("typeahead:change", function(aEvent, aSuggestion) {
        if (!selected) {
            $myTypeahead.typeahead("val", originalVal);
        }
      });   

    });
  } );
} )( jQuery );