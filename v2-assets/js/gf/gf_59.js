( function( $ ) {
  $( document ).ready(function() {
 
    $("#label_59_3_0").after(function() {
      return "<ul style='padding-left: 20px; font-size: 13px; margin-top: 10px; font-style: italic;'><li>Any site member can join this group.</li><li>This group will be listed in the groups directory and in search results.</li><li>Group content and activity will be visible to any site member.</li></ul>";
    });
    $("#label_59_3_1").after(function() {
      return "<ul style='padding-left: 20px; font-size: 13px; margin-top: 10px; font-style: italic;'><li>Only users who request membership and are accepted can join the group.</li><li>This group will be listed in the groups directory and in search results.</li><li>Group content and activity will only be visible to members of the group.</li></ul>";
    });
    $("#label_59_3_2").after(function() {
      return "<ul style='padding-left: 20px; font-size: 13px; margin-top: 10px; font-style: italic;'><li>Only users who are invited can join the group.</li><li>This group will not be listed in the groups directory or search results.</li><li>Group content and activity will only be visible to members of the group.</li></ul>";
    });

    var substringMatcher = function(strs) {
      console.log('59');
      return function findMatches(q, cb) {
        var matches, substringRegex;
    
        // an array that will be populated with substring matches
        matches = [];
    
        // regex used to determine if a string contains the substring `q`
        substrRegex = new RegExp(q, 'i');
    
        // iterate through the pool of strings and for any string that
        // contains the substring `q`, add it to the `matches` array
        $.each(strs, function(i, str) {
          if (substrRegex.test(str)) {
            matches.push(str);
          }
        });
    
        cb(matches);
      };
    };
    
    $.get('/wp-admin/admin-ajax.php', {action:'get_group'}, function (data, textStatus, jqXHR) {
      // $('p').append(data.firstName);
      console.log(data);

      var type = $('#input_59_19').val();
      var states = data.data.group_lists[type];
      // var states = data.data.all_group_lists;
      
	  console.log(states);

      $( "#choice_59_18_0" ).change(function() {
        console.log('change choice_59_18_0');
        changeType($("#choice_59_18_0").val());
      });
      $( "#choice_59_18_1" ).change(function() {
        console.log('change choice_59_18_1');
        // console.log($("#choice_59_18_1").val());
        changeType($("#choice_59_18_1").val());
      });
      $( "#choice_59_18_2" ).change(function() {
        console.log('change choice_59_18_2');
        changeType($("#choice_59_18_2").val());
      });

      function changeType(type) {
        var states = data.data.group_lists[type];
        // var states = data.data.all_group_lists;

        $('.typeahead #input_59_12').typeahead('destroy');
        $('.typeahead #input_59_12').typeahead({
          hint: true,
          highlight: true,
          minLength: 1
        },
        {
          name: 'states',
          source: substringMatcher(states),
          templates: {
            empty: function(context){
              $(".tt-dataset").text('No Results Found');
            }
          }
        });
      };

      // $( "#input_59_18" ).change(function() {
      //   faith = $('#input_59_18').val();
      //   var states = data.data.group_lists[faith];
      //   var states = data.data.all_group_lists;

      //   $('.typeahead #input_59_12').typeahead('destroy');
      //   $('.typeahead #input_59_12').typeahead({
      //     hint: true,
      //     highlight: true,
      //     minLength: 1
      //   },
      //   {
      //     name: 'states',
      //     source: substringMatcher(states),
      //     templates: {
      //       empty: function(context){
      //         $(".tt-dataset").text('No Results Found');
      //       }
      //     }
      //   });
      // });

      $('.typeahead #input_59_12').typeahead({
        hint: false,
        highlight: true,
        minLength: 1
      },
      {
        name: 'states',
        source: substringMatcher(states),
        templates: {
          empty: function(context){
            $(".tt-dataset").text('No Results Found');
          }
        }
      });
      
      var $myTypeahead = $(".typeahead #input_59_12");
      var selected      = null;
      var originalVal   = null;
      $myTypeahead.on("typeahead:active", function(aEvent) {
        selected       = null;
        originalVal    = $myTypeahead.typeahead("val");
      })
      $myTypeahead.on("typeahead:select", function(aEvent, aSuggestion) {
        selected = aSuggestion;
      });
      $myTypeahead.on("typeahead:change", function(aEvent, aSuggestion) {
        if (!selected) {
            $myTypeahead.typeahead("val", originalVal);
        }
      });   

    });
  } );
} )( jQuery );