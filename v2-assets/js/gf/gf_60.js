( function( $ ) {
  $( document ).ready(function() {
 
    $("#label_60_3_0").after(function() {
      return "<ul style='padding-left: 20px; font-size: 13px; margin-top: 10px; font-style: italic;'><li>Any site member can join this group.</li><li>This group will be listed in the groups directory and in search results.</li><li>Group content and activity will be visible to any site member.</li></ul>";
    });
    $("#label_60_3_1").after(function() {
      return "<ul style='padding-left: 20px; font-size: 13px; margin-top: 10px; font-style: italic;'><li>Only users who request membership and are accepted can join the group.</li><li>This group will be listed in the groups directory and in search results.</li><li>Group content and activity will only be visible to members of the group.</li></ul>";
    });
    $("#label_60_3_2").after(function() {
      return "<ul style='padding-left: 20px; font-size: 13px; margin-top: 10px; font-style: italic;'><li>Only users who are invited can join the group.</li><li>This group will not be listed in the groups directory or search results.</li><li>Group content and activity will only be visible to members of the group.</li></ul>";
    });
	
    var substringMatcher = function(strs) {
      return function findMatches(q, cb) {
        var matches, substringRegex;
    
        // an array that will be populated with substring matches
        matches = [];
    
        // regex used to determine if a string contains the substring `q`
        substrRegex = new RegExp(q, 'i');
    
        // iterate through the pool of strings and for any string that
        // contains the substring `q`, add it to the `matches` array
        $.each(strs, function(i, str) {
          if (substrRegex.test(str)) {
            matches.push(str);
          }
        });
    
        cb(matches);
      };
    };
    
    $.get('/wp-admin/admin-ajax.php', {action:'get_group'}, function (data, textStatus, jqXHR) {
      // console.log(data);

      var type = $('#input_60_25').val();
      var states = data.data.group_lists[type];
      
      $( "#choice_60_24_0" ).change(function() {
        changeType($("#choice_60_24_0").val());
      });
      $( "#choice_60_24_1" ).change(function() {
        changeType($("#choice_60_24_1").val());
      });
      $( "#choice_60_24_2" ).change(function() {
        changeType($("#choice_60_24_2").val());
      });

      function changeType(type) {
        var states = data.data.group_lists[type];
        
        $('.typeahead #input_60_12').typeahead('destroy');
        $('.typeahead #input_60_12').typeahead({
          hint: true,
          highlight: true,
          minLength: 1
        },
        {
          name: 'states',
          source: substringMatcher(states),
          templates: {
            empty: function(context){
              $(".tt-dataset").text('No Results Found');
            }
          }
        });
      };

      $('.typeahead #input_60_12').typeahead({
        hint: false,
        highlight: true,
        minLength: 1
      },
      {
        name: 'states',
        source: substringMatcher(states),
        templates: {
          empty: function(context){
            $(".tt-dataset").text('No Results Found');
          }
        }
      });
      
      var $myTypeahead = $(".typeahead #input_60_12");
      var selected      = null;
      var originalVal   = null;
      $myTypeahead.on("typeahead:active", function(aEvent) {
        selected       = null;
        originalVal    = $myTypeahead.typeahead("val");
      })
      $myTypeahead.on("typeahead:select", function(aEvent, aSuggestion) {
        selected = aSuggestion;
      });
      $myTypeahead.on("typeahead:change", function(aEvent, aSuggestion) {
        if (!selected) {
            $myTypeahead.typeahead("val", originalVal);
        }
      });   

    });

  } );
} )( jQuery );