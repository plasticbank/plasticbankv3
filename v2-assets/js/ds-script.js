// Custom JS goes here ------------


// Dropdown field REQUIRES the custom CSS Class of js-membership-select
function nameMembershipFields() {
	var membershipSelect = document.getElementsByClassName('js-membership-selector')[0] || false;
	if (membershipSelect) {
		var membershipSelectField = membershipSelect.getElementsByTagName('SELECT')[0];
		membershipSelectField.setAttribute('name','membership_select');
		var membershipField = document.querySelectorAll('.js-membership-field input')[0];
		membershipSelectField.addEventListener('change',function(){
			membershipField.value = membershipSelectField.options[membershipSelectField.selectedIndex].text;
		});
	}
} window.addEventListener('load', nameMembershipFields, false);


// Apply the lookup to the hidden field OR the value from the dropdown to the hidden field
function hiddenCountryField() {
	var countryField = document.querySelectorAll('.js-country-field input')[0] || false; //assumes only one on a page
	if (countryField) {
		jQuery.ajax('http://ip-api.com/json').then(
			function success(response) {
				// window.console && console.log('User\'s Location Data is ', response);
				countryField.value = response.country;
			},
			function fail(data, status) {
				// window.console && console.log('Request failed.  Returned status of', status);
				var countrySelect = document.querySelectorAll('.js-country-select select')[0];
				countrySelect.addEventListener('change', function() {
					countryField.value = countrySelect.value;
				});
			}
		);
	}
} window.addEventListener('load', hiddenCountryField, false);


// Hide unnecessary buttons on Lifter Student Dashboard
function hideDashboardButtons() {
	var is_dashboard = document.querySelectorAll('.llms-student-dashboard.dashboard');
	if (is_dashboard.length > 0) {
		var dashboardSections = document.querySelectorAll('.llms-sd-section');
		for (var i=0; i<dashboardSections.length; i++) {
			var section = dashboardSections[i];
			if ( section.querySelectorAll('ul.llms-loop-list').length == 0 ) {
				section.querySelectorAll('footer.llms-sd-section-footer')[0].remove();
			}
		}
	}
} window.addEventListener('load', hideDashboardButtons, false);