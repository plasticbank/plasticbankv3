(function ($) {
  $(document).ready(function () {
    // Qty.
    $(".pb-qty-plus").on("click", function (e) {
      $input = $(".quantity > .qty");
      var val = parseInt($input.val());
      var step = $input.attr("step");
      step = "undefined" !== typeof step ? parseInt(step) : 1;
      $input.val(val + step).change();
    });
    $(".pb-qty-minus").on("click", function (e) {
      $input = $(".quantity > .qty");
      var val = parseInt($input.val());
      var step = $input.attr("step");
      step = "undefined" !== typeof step ? parseInt(step) : 1;
      if (val > 0) {
        $input.val(val - step).change();
      }
    });
    // EO Qty.

    // Load More.
    $(".more").slice(0, 4).show();
    if ($(".partner:hidden").length != 0) {
      $("#loadMore").show();
    }
    $(".more").addClass("hide");
    $("#loadMore").on("click", function (e) {
      e.preventDefault();
      $(".more:hidden").slice(0, 4).slideDown();
      if ($(".more:hidden").length == 0) {
        $("#loadMore").fadeOut("slow");
      }
    });
    //EO Load more.

    // Media Kit
    $(".media-gallery .modula-item").each(function (index, element) {
      $(this).append(
        '<div class="btn-download"><a href="" download="plasticbank.jpg"><img src="https://plasticbank.com/wp-content/uploads/download.svg" /></a></div>'
      );
      $(this)
        .find(".btn-download > a")
        .attr("href", $(this).find(".modula-item-link").attr("href"));
    });
    $(".btn-toggle-show").click(function (e) {
      e.preventDefault();
      $(this)
        .parent()
        .siblings()
        .find("a.btn-media-kit.child")
        .attr("style", "display:block!important");
      $(this)
        .parent()
        .siblings()
        .find("a.btn-toggle-hide")
        .attr("style", "display:inline-block!important");
      $(this).attr("style", "display:none!important");
    });
    $(".btn-toggle-hide").click(function (e) {
      e.preventDefault();
      $(this)
        .parent()
        .siblings()
        .find("a.btn-media-kit.child")
        .attr("style", "display:none!important");
      $(this)
        .parent()
        .siblings()
        .find("a.btn-toggle-show")
        .attr("style", "display:inline-block!important");
      $(this).attr("style", "display:none!important");
    });
    $(this).closest(".et_pb_columns").find(".btn-media-kit");
    // End of Media Kit.

    // Mobile Menu.
    $(".mobile_menu_bar_toggle").click(function () {
      $("body").toggleClass("mobile-menu-open");
    });
    // EO Mobile Menu.

    // Cart.
    $(".cart-toggle").click(function () {
      $(".pb-cart-sidebar").toggleClass("opened");
      $(".pb-cart-overlay").toggleClass("opened");
    });
    $(".pb-cart-hide-toggle").click(function () {
      $(".pb-cart-sidebar").removeClass("opened");
      $(".pb-cart-overlay").removeClass("opened");
    });
    $(".pb-cart-overlay").click(function () {
      $(".pb-cart-sidebar").removeClass("opened");
      $(".pb-cart-overlay").removeClass("opened");
    });
    $("body").on("added_to_cart", function () {
      $(".pb-cart-sidebar").toggleClass("opened");
      $(".pb-cart-overlay").removeClass("loading");
    });
    // EO Cart.

    // Product Qty.
    function changeProductQTY() {
      var qty = $('form .quantity input[type="number"]').val();
      var price = $(".single_variation_wrap span.price .woocommerce-Price-amount").text();
      var symbol_only = $("#current_currecy_symbol").val();
      var price_only = $("#pb_product_price").val();
      var currency_only = $("#current_currecy").val();
      var product_id = $("#pb_product_id").val();
      var product_id_alt = $(".product_id").val();
      var $form = $("#gform_29");

      if (product_id == 15260 || product_id == 93931 || product_id == 93915) {
        qty = $('form .quantity input[type="number"]').val();
      } else if (
        product_id == 15847 ||
        product_id == 15850 ||
        product_id == 15806 ||
        product_id == 93789 ||
        product_id == 93763 ||
        product_id == 93801 ||
        product_id == 93792 ||
        product_id == 93812 ||
        product_id == 93803 ||
        product_id == 96565 ||
        product_id == 96562 ||
        product_id == 96560
      ) {
        qty = $('form .quantity input[type="number"]').val();
        var for_gift = $form.find("input[name=input_23]:checked").val() || "";
        if (for_gift == "Buy for myself") {
          qty = 1;
        } else {
          qty = $("#field_29_14").find("table.gfield_list tbody tr").length;
        }
      } else if (product_id == 26074 || product_id == 94054 || product_id == 94074) {
        qty = $("form #input_53_1").val();
        qty = qty.replace(".00", "");
        qty = qty.replace(symbol_only, "");
        qty = qty.replace(",", "");
        qty = Number(qty.replace(symbol_only, ""));
      } else if (product_id == 73329 || product_id_alt == 73329) {
        qty = $("form #input_68_1").val();
        qty = qty.replace(".00", "");
        qty = qty.replace(",", "");
        qty = Number(qty.replace(symbol_only, ""));
      } else {
        // price = price.replace(symbol_only, "");
        // qty = Number(qty.replace(",", ""));
        // price_only = price.substring(0, price.length - 4);
      }

      total_price = price_only * qty;
      
      if (product_id == 94887) {
        $(".single_add_to_cart_button").html("Contribute");
      } else if (product_id == 73329) {
        $(".single_add_to_cart_button").html("Continue");
      } else if (
        product_id == 111255 ||
        product_id == 111257 ||
        product_id == 111258 ||
        product_id == undefined
      ) {
				$(".single_add_to_cart_button").html("Purchase");
			} else {
        $(".single_add_to_cart_button").html(
          "Add to Cart - " + symbol_only + total_price.toFixed(2) + " " + currency_only
        );
      }
    }
    changeProductQTY();
    $('form .quantity input[type="number"]').change(changeProductQTY);
    $('form .quantity input[type="number"]').keyup(changeProductQTY);
    $(".variable-item").click(changeProductQTY);

    $("form #input_53_1").change(changeProductQTY);
    $("form #input_53_1").keyup(changeProductQTY);

    $("form #input_68_1").change(changeProductQTY);
    $("form #input_68_1").keyup(changeProductQTY);
    // EO Product Qty.

    // Owl Carousel.
    $(".pb-six-columns-carousel").owlCarousel({
      loop: true,
      margin: 70,
      responsiveClass: true,
      autoplay: false,
      autoplayTimeout: 3000,
      autoplayHoverPause: true,
      responsive: {
        0: {
          margin: 30,
          items: 2,
          dots: false,
        },
        600: {
          margin: 50,
          items: 4,
          dots: false,
        },
        980: {
          items: 6,
          dots: false,
          loop: false,
        },
      },
    });
    var hv = $("#home-video");
    hv.owlCarousel({
      loop: true,
      margin: 30,
      responsiveClass: true,
      responsive: {
        0: {
          items: 1,
          dots: true,
        },
        600: {
          items: 2,
          dots: true,
        },
        980: {
          items: 2.5,
          dots: false,
          loop: true,
        },
      },
    });

    $(".owl-prev-trigger").click(function (e) {
      hv.trigger("prev.owl.carousel");
      $(".owl-next-trigger").removeClass("active");
      $(".owl-prev-trigger").addClass("active");
      $("body").click(function () {
        $(".owl-prev-trigger").removeClass("active");
        $("body").off("click"); // cancel the body's click handler when it's used
      });
      e.stopPropagation();
    });
    $(".owl-next-trigger").click(function (e) {
      hv.trigger("next.owl.carousel");
      $(".owl-prev-trigger").removeClass("active");
      $(".owl-next-trigger").addClass("active");
      $("body").click(function () {
        $(".owl-next-trigger").removeClass("active");
        $("body").off("click"); // cancel the body's click handler when it's used
      });
      e.stopPropagation();
    });

    var partners = $("#partners");
    partners.owlCarousel({
      loop: true,
      margin: 30,
      responsiveClass: true,
      autoplay: true,
      autoplayTimeout: 3000,
      autoplayHoverPause: true,
      responsive: {
        0: {
          items: 2,
          dots: true,
        },
        600: {
          items: 3,
          dots: true,
        },
        980: {
          items: 3,
          dots: false,
          loop: true,
        },
      },
    });

    var partners = $("#single-carousel");
    partners.owlCarousel({
      loop: true,
      margin: 30,
      nav: true,
      navText: [
        "<a class='owl-nav-trigger prev owl-prev-trigger'></a>",
        "<a class='owl-nav-trigger next owl-next-trigger'></a>",
      ],
      responsiveClass: true,
      autoplay: true,
      autoplayTimeout: 3000,
      autoplayHoverPause: true,
      dots: false,
      responsive: {
        0: {
          items: 1,
        },
        600: {
          items: 1,
        },
        980: {
          items: 1,
        },
      },
    });

    var partners = $("#fp-purpose");
    partners.owlCarousel({
      loop: true,
      margin: 20,
      responsiveClass: true,
      autoplay: true,
      autoplayTimeout: 3000,
      autoplayHoverPause: true,
      dots: false,
      responsive: {
        0: {
          items: 3,
          margin: 20,
          dots: true,
          autoplay: true,
        },
        600: {
          items: 3,
          margin: 30,
          dots: true,
          autoplay: true,
        },
        980: {
          items: 5,
          margin: 50,
          autoplay: false,
        },
      },
    });

    var solutions = $("#business-solutions");
    solutions.owlCarousel({
      loop: false,
      margin: 20,
      responsiveClass: true,
      autoplay: false,
      autoplayTimeout: 3000,
      autoplayHoverPause: true,
      dots: false,
      responsive: {
        0: {
          items: 3,
          margin: 20,
        },
        600: {
          items: 3,
          margin: 30,
        },
        980: {
          items: 3,
          margin: 50,
        },
      },
    });

    var programs = $("#programs");
    programs.owlCarousel({
      loop: false,
      margin: 20,
      animateOut: "fadeOut",
      dotsContainer: "#program-selectors",
      responsiveClass: true,
      autoplay: false,
      dots: true,
      responsive: {
        0: {
          items: 1,
          margin: 20,
          autoHeight: true,
          autoplay: false,
        },
        600: {
          items: 1,
          margin: 30,
          autoHeight: true,
          autoplay: false,
        },
        980: {
          items: 1,
          margin: 30,
          autoHeight: true,
          autoplay: false,
        },
      },
    });

    $("#program-selectors").on("click", "li", function (e) {
      programs.trigger("to.owl.carousel", [$(this).index(), 300]);
    });

    $("#program-prev-trigger").click(function (e) {
      programs.trigger("prev.owl.carousel");
      $("#program-next-trigger").removeClass("active");
      $("#program-prev-trigger").addClass("active");
      $("body").click(function () {
        $("#program-prev-trigger").removeClass("active");
        $("body").off("click");
      });
      e.stopPropagation();
    });
    $("#program-next-trigger").click(function (e) {
      programs.trigger("next.owl.carousel");
      $("#program-prev-trigger").removeClass("active");
      $("#program-next-trigger").addClass("active");
      $("body").click(function () {
        $("#program-next-trigger").removeClass("active");
        $("body").off("click");
      });
      e.stopPropagation();
    });

    var ac = $("#awards-carousel");
    ac.owlCarousel({
      loop: true,
      margin: 30,
      responsiveClass: true,
      responsive: {
        0: {
          stagePadding: 70,
          items: 1,
          dots: true,
        },
        600: {
          stagePadding: 70,
          items: 1,
          dots: true,
        },
        980: {
          items: 2.5,
          dots: false,
        },
      },
    });

    $("#ac-prev-trigger").click(function (e) {
      ac.trigger("prev.owl.carousel");
      $("#ac-next-trigger").removeClass("active");
      $("#ac-prev-trigger").addClass("active");
      $("body").click(function () {
        $("#ac-prev-trigger").removeClass("active");
        $("body").off("click");
      });
      e.stopPropagation();
    });
    $("#ac-next-trigger").click(function (e) {
      ac.trigger("next.owl.carousel");
      $("#ac-prev-trigger").removeClass("active");
      $("#ac-next-trigger").addClass("active");
      $("body").click(function () {
        $("#ac-next-trigger").removeClass("active");
        $("body").off("click");
      });
      e.stopPropagation();
    });

    var tc = $("#team-carousel");
    tc.owlCarousel({
      loop: true,
      margin: 30,
      responsiveClass: true,
      responsive: {
        0: {
          stagePadding: 70,
          items: 1,
          dots: true,
        },
        600: {
          stagePadding: 70,
          items: 2,
          dots: true,
        },
        980: {
          items: 3.5,
          dots: false,
        },
      },
    });

    $("#tc-prev-trigger").click(function (e) {
      tc.trigger("prev.owl.carousel");
      $("#tc-next-trigger").removeClass("active");
      $("#tc-prev-trigger").addClass("active");
      $("body").click(function () {
        $("#tc-prev-trigger").removeClass("active");
        $("body").off("click");
      });
      e.stopPropagation();
    });
    $("#tc-next-trigger").click(function (e) {
      tc.trigger("next.owl.carousel");
      $("#tc-prev-trigger").removeClass("active");
      $("#tc-next-trigger").addClass("active");
      $("body").click(function () {
        $("#tc-next-trigger").removeClass("active");
        $("body").off("click");
      });
      e.stopPropagation();
    });

    var pc = $("#partners-carousel");
    pc.owlCarousel({
      loop: true,
      margin: 30,
      responsiveClass: true,
      responsive: {
        0: {
          stagePadding: 70,
          items: 1,
          dots: true,
        },
        600: {
          stagePadding: 70,
          items: 1,
          dots: true,
        },
        980: {
          items: 2.5,
          dots: false,
        },
      },
    });

    $("#pc-prev-trigger").click(function (e) {
      pc.trigger("prev.owl.carousel");
      $("#pc-next-trigger").removeClass("active");
      $("#pc-prev-trigger").addClass("active");
      $("body").click(function () {
        $("#pc-prev-trigger").removeClass("active");
        $("body").off("click");
      });
      e.stopPropagation();
    });
    $("#pc-next-trigger").click(function (e) {
      pc.trigger("next.owl.carousel");
      $("#pc-prev-trigger").removeClass("active");
      $("#pc-next-trigger").addClass("active");
      $("body").click(function () {
        $("#pc-next-trigger").removeClass("active");
        $("body").off("click");
      });
      e.stopPropagation();
    });

    var testimonies = $("#testimonies");
    testimonies.owlCarousel({
      loop: true,
      margin: 20,
      animateOut: "fadeOut",
      nav: false,
      responsiveClass: true,
      autoplay: true,
      autoplayTimeout: 3000,
      autoplayHoverPause: true,
      dotsContainer: "#testimony-photos",
      dots: true,
      responsive: {
        0: {
          items: 1,
          margin: 20,
          autoHeight: true,
          nav: true,
          navText: [
            "<a class='owl-nav-trigger prev owl-prev-trigger'></a>",
            "<a class='owl-nav-trigger next owl-next-trigger'></a>",
          ],
          autoplay: false,
        },
        600: {
          items: 1,
          margin: 30,
          autoHeight: true,
          nav: true,
          navText: [
            "<a class='owl-nav-trigger prev owl-prev-trigger'></a>",
            "<a class='owl-nav-trigger next owl-next-trigger'></a>",
          ],
          autoplay: false,
        },
        980: {
          items: 1,
          margin: 30,
          nav: true,
          navText: [
            "<a class='owl-nav-trigger prev owl-prev-trigger'></a>",
            "<a class='owl-nav-trigger next owl-next-trigger'></a>",
          ],
        },
      },
    });

    var partners = $("#client-infographs");
    partners.owlCarousel({
      loop: false,
      margin: 30,
      nav: false,
      navText: [
        "<a class='owl-nav-trigger prev owl-prev-trigger'></a>",
        "<a class='owl-nav-trigger next owl-next-trigger'></a>",
      ],
      responsiveClass: true,
      autoplay: false,
      autoplayTimeout: 3000,
      autoplayHoverPause: true,
      dots: true,
      responsive: {
        0: {
          items: 1,
        },
        600: {
          items: 1,
        },
        980: {
          items: 1,
          nav: true,
        },
      },
    });
    //EO Owl Carousel.
    function validateEmail(email) {
      const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    }

    // Footer.
    var element = $("#page-container");
    var height = element.offsetHeight;
    if (height < screen.height) {
      $("#the-footer").classList.add("stikybottom");
    }
  });
})(jQuery);
