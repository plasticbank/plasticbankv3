module.exports = {
	content: ["./**/*.{php,html,js}"],
	theme: {
		extend: {
			fontFamily: {
				sans: ["Inter", "sans-serif"],
				heading: ["Hind", "sans-serif"],
				mono: ["monospace"],
				icon: ["Plasticbank"],
			},
			lineHeight: {
				11: "2.75rem",
				12: "3rem",
			},
			colors: {
				blue: {
					50: "#F6FCFD",
					100: "#D3F5F7",
					200: "#A8EAEF",
					300: "#65D8DF",
					DEFAULT: "#1CB8CC",
					500: "#10899E",
					600: "#08636A",
				},
				green: {
					50: "#F7FFED",
					100: "#E2F6C5",
					200: "#C6F08B",
					DEFAULT: "#98CA53",
					400: "#71AE1B",
					500: "#5C9926",
					600: "#396C0B",
				},
				neutral: {
					0: "#ffffff",
					50: "#f9fafc",
					100: "#eeeeee",
					200: "#e0e0e0",
					DEFAULT: "#cccccc",
					400: "#acacac",
					500: "#777778",
					600: "#616162",
					700: "#484c4c",
					800: "#1e1e1e",
					900: "#000000",
				},
			},
		},
	},
	plugins: [],
};
