## Plastic Bank 3.0

- git clone this repo
- `npm i`

Develop: `npx tailwindcss -i ./tailwind/main.css -o ./app.css --watch --postcss`

Production: `npx tailwindcss -o app.css --postcss --minify`

All CSS development should happen in `tailwind/main.css`

Resources:

- https://tailwindcss.com/
- https://tailwindcss.com/blog/tailwindcss-v3
- https://underscores.me/
